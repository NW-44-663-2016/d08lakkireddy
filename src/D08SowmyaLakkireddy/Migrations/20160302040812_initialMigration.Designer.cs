using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using D08SowmyaLakkireddy.Models;

namespace D08SowmyaLakkireddy.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20160302040812_initialMigration")]
    partial class initialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("D08SowmyaLakkireddy.Models.Location", b =>
                {
                    b.Property<int>("LocationID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("D08SowmyaLakkireddy.Models.ShoppingMall", b =>
                {
                    b.Property<int>("MallID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("EmaidId");

                    b.Property<string>("MallName");

                    b.Property<string>("PhoneNumber");

                    b.Property<string>("Place");

                    b.Property<string>("WebsiteURL");

                    b.Property<int>("YearEstablished");

                    b.HasKey("MallID");
                });
        }
    }
}
