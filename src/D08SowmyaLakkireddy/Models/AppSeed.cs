﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace D08SowmyaLakkireddy.Models
{
    public static  class AppSeed
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (context == null) { return; }
            if(context.Locations.Any() ) { return; }

            var Locationlst = new List<Location>()
            {
            new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" },
            new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" },
            new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" },
            new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" },
            new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" },
            new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" },
            };
            context.Locations.AddRange(Locationlst);

            var ShoppingMalllst = new List<ShoppingMall>()
            {
             new ShoppingMall() { MallName = "Walmart", EmaidId = "walmart@gmail.com", PhoneNumber = " 4537654290" , Place = "Maryville" ,YearEstablished = 1970 ,WebsiteURL = "http://www.walmart.com/" },
            new ShoppingMall() { MallName = "JCPenney", EmaidId = "jcpenney@gmail.com", PhoneNumber = " 7653901954", Place = "Denver", YearEstablished = 1985, WebsiteURL = "http://www.jcpenney.com/" },
            new ShoppingMall() { MallName = "Rue 21", EmaidId = "rue 21@gmail.com", PhoneNumber = " 2074370841", Place = "Orlando", YearEstablished = 1976, WebsiteURL = "http://www.rue21.com/" },
            new ShoppingMall() { MallName = "Aeropostale", EmaidId = "aeropostale@gmail.com", PhoneNumber = " 5024860435", Place = "Maryville", YearEstablished = 1973, WebsiteURL = "http://www.aeropostale.com/" },
            new ShoppingMall() { MallName = "Hollister", EmaidId = "hollister@gmail.com", PhoneNumber = " 6693450254", Place = "Maryville", YearEstablished = 2000, WebsiteURL = "http://www.hollister.com/" },
            new ShoppingMall() { MallName = "Gap", EmaidId = "gap@gmail.com", PhoneNumber = " 9458720467", Place = "Sanfrancisco", YearEstablished = 1969, WebsiteURL = "http://www.gap.com/" },
            new ShoppingMall() { MallName = "H & M", EmaidId = "H&M@gmail.com", PhoneNumber = " 8437601635", Place = "Maryville", YearEstablished = 1981, WebsiteURL = "http://www.H&M.com/" },
            };
            context.ShoppingMalls.AddRange(ShoppingMalllst);
            context.SaveChanges();

        }
    }
}
