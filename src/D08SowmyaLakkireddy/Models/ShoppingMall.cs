﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace D08SowmyaLakkireddy.Models
{
    public class ShoppingMall
    {
        [Key]
        [ScaffoldColumn(false)]
        public int MallID { get; set; }

        [Display(Name = "MallName")]
        public string MallName { get; set; }

        [Display(Name = "Email Id")]
        public String EmaidId { get; set; }

        [RegularExpression(@"^((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}$")]
        [Display(Name = "Phone Number")]
        public String PhoneNumber { get; set; }

        [Display(Name = "Place")]
        public String Place { get; set; }

        [Display(Name = "Year Established")]
        public int YearEstablished { get; set; }

        [RegularExpression("^ http(s) ?://([\\w-]+.)+[\\w-]+(/[\\w- ./?%&=])?$")]
        [Display(Name = "Website")]
        public String WebsiteURL { get; set; }
    }
}
