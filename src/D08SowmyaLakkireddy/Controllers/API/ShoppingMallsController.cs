using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using D08SowmyaLakkireddy.Models;

namespace D08SowmyaLakkireddy.Controllers
{
    [Produces("application/json")]
    [Route("api/ShoppingMalls")]
    public class ShoppingMallsController : Controller
    {
        private ApplicationDbContext _context;

        public ShoppingMallsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/ShoppingMalls
        [HttpGet]
        public IEnumerable<ShoppingMall> GetShoppingMalls()
        {
            return _context.ShoppingMalls;
        }

        // GET: api/ShoppingMalls/5
        [HttpGet("{id}", Name = "GetShoppingMall")]
        public IActionResult GetShoppingMall([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            ShoppingMall shoppingMall = _context.ShoppingMalls.Single(m => m.MallID == id);

            if (shoppingMall == null)
            {
                return HttpNotFound();
            }

            return Ok(shoppingMall);
        }

        // PUT: api/ShoppingMalls/5
        [HttpPut("{id}")]
        public IActionResult PutShoppingMall(int id, [FromBody] ShoppingMall shoppingMall)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != shoppingMall.MallID)
            {
                return HttpBadRequest();
            }

            _context.Entry(shoppingMall).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShoppingMallExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/ShoppingMalls
        [HttpPost]
        public IActionResult PostShoppingMall([FromBody] ShoppingMall shoppingMall)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.ShoppingMalls.Add(shoppingMall);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ShoppingMallExists(shoppingMall.MallID))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetShoppingMall", new { id = shoppingMall.MallID }, shoppingMall);
        }

        // DELETE: api/ShoppingMalls/5
        [HttpDelete("{id}")]
        public IActionResult DeleteShoppingMall(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            ShoppingMall shoppingMall = _context.ShoppingMalls.Single(m => m.MallID == id);
            if (shoppingMall == null)
            {
                return HttpNotFound();
            }

            _context.ShoppingMalls.Remove(shoppingMall);
            _context.SaveChanges();

            return Ok(shoppingMall);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShoppingMallExists(int id)
        {
            return _context.ShoppingMalls.Count(e => e.MallID == id) > 0;
        }
    }
}